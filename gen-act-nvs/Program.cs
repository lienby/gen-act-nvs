﻿using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Engines;
using Org.BouncyCastle.Crypto.Macs;
using Org.BouncyCastle.Crypto.Parameters;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gen_act_nvs
{
    class Program
    {
        static byte[] ACT_NVS_KEY = new byte[] { 0x5A, 0x91, 0xFC, 0x74, 0xA8, 0x2B, 0xE3, 0xF2, 0xB8, 0xF4, 0xDB, 0x60, 0x70, 0xA0, 0x99, 0xA2, 0xBD, 0xF0, 0x0E, 0x7B, 0xF0, 0x0E, 0x7B, 0xF0, 0x8B, 0x68, 0x55, 0x34, 0xA0, 0x64, 0x6D, 0x87 };
        static void WriteString(Stream stream, string str)
        {
            byte[] strBytes = Encoding.UTF8.GetBytes(str);
            stream.Write(strBytes, 0x00, strBytes.Length);
            stream.WriteByte(0x00);
        }

        static void WriteInt32(Stream stream, Int32 numb)
        {
            byte[] intBytes = BitConverter.GetBytes(numb);
            stream.Write(intBytes, 0x00, intBytes.Length);
        }
        static Int32 ReadInt32(Stream stream)
        {
            byte[] intBytes = new byte[0x4];
            stream.Read(intBytes, 0x00, intBytes.Length);
            return BitConverter.ToInt32(intBytes, 0x00);
        }

        static void Main(string[] args)
        {
            if (args.Length >= 1)
            {
                string actPath = args[0];
                string actNvsPath = "";
                if (args.Length >= 2)
                {
                    actNvsPath = args[1];
                }
                else
                {
                    actNvsPath = Path.Combine(Path.GetDirectoryName(actPath), "act-nvs.dat");
                }


                

                if (File.Exists(actNvsPath))
                {
                    Console.WriteLine(actNvsPath + " Allready exists!");
                    return;
                }

                FileStream act = new FileStream(actPath, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                FileStream nvsAct = new FileStream(actNvsPath, FileMode.CreateNew, FileAccess.ReadWrite);

                act.Seek(0x8, SeekOrigin.Begin);
                int IssueNo = ReadInt32(act);
                int StartDate = ReadInt32(act);
                int EndDate = ReadInt32(act);

               
                WriteString(nvsAct, "act");
                WriteInt32(nvsAct, IssueNo);
                WriteInt32(nvsAct, EndDate);
                WriteInt32(nvsAct, StartDate);

                Console.WriteLine("Writing nvs-act.dat\n");
                Console.WriteLine("Issue No. " + IssueNo.ToString());
                Console.WriteLine("End Date " + EndDate.ToString());
                Console.WriteLine("Start Date " + StartDate.ToString());
                Console.WriteLine("\nCaclulating CMAC");

                //Calculate CMAC
                nvsAct.Seek(0x00, SeekOrigin.Begin);
                byte[] actBuffer = new byte[0x10];
                nvsAct.Read(actBuffer, 0x00, actBuffer.Length);

                IBlockCipher cipher = new AesEngine();
                IMac mac = new CMac(cipher, 128);
                KeyParameter key = new KeyParameter(ACT_NVS_KEY);
                mac.Init(key);
                mac.BlockUpdate(actBuffer, 0, actBuffer.Length);
                byte[] outBytes = new byte[16];
                mac.DoFinal(outBytes, 0);

                nvsAct.Seek(0x10, SeekOrigin.Begin);
                nvsAct.Write(outBytes,0x00,outBytes.Length);

                foreach(byte b in outBytes)
                {
                    Console.Write(b.ToString("X") + " ");
                }
                Console.WriteLine("\nDone!");
            }
            else
            {
                Console.WriteLine("gen-act-nvs <input act> [output act]");
            }
        }
    }
}
